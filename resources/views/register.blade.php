<!DOCTYPE html>
<html>
  <head>
    <title> SanberBook - Buat Account Baru! </title>
  <head>

  <body>

    <div>
      <h1> Buat Account Baru! </h1>
      <h3> Sign Up Form </h3>

      <form action="/welcome" method="post">
        @csrf
        <label for="first"> First name: </label><br><br>
        <input type="text" id="first" name="depan">
        <br><br>
        <label for="last"> Last name: </label><br><br>
        <input type="text" id="last" name="belakang">
        <br><br>
        <label> Gender: </label><br><br>
        <input type="radio" name="gender" value="0"> Male <br>
        <input type="radio" name="gender" value="1"> Female <br>
        <input type="radio" name="gender" value="2"> Other
        <br><br>
        <label> Nationality: </label><br><br>
        <select>
          <option value="id">Indonesian</option>
          <option value="sg">Singaporean</option>
          <option value="my">Malaysian</option>
          <option value="au">Australian</option>
        </select>
        <br><br>
        <label> Language Spoken: </label><br><br>
        <input type="checkbox" name="language" value="0"> Bahasa Indonesia <br>
        <input type="checkbox" name="language" value="1"> English <br>
        <input type="checkbox" name="language" value="2"> Other
        <br><br>
        <label for="bio"> Bio: </label><br><br>
        <textarea rows="7" id="bio"></textarea>

        <br>
        <input type="submit" value="Sign Up">
      </form>
    </div>

  </body>
</html>
