<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
      return view('register');
    }

    public function welcome(Request $request)
    {
      $depan = $request["depan"];
      $belakang = $request["belakang"];
      return view('welcome (1)', ["depan" => $depan, "belakang" => $belakang]);
    }
}
