<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function index()
    {
      $pertanyaan = DB::table('pertanyaan')->get();
      return view('pertanyaan.index', compact('pertanyaan'));
    }

    public function create()
    {
      return view('pertanyaan.create');
    }

    public function store(Request $request)
    {
      $request->validate([
        'judul' => 'required|unique:pertanyaan',
        'isi' => 'required'
      ]);
      $query = DB::table('pertanyaan')->insert([
        "judul" => $request["judul"],
        "isi" => $request["isi"]
      ]);

      return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Disimpan!');
    }

    public function show($pertanyaan_id)
    {
      $question = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
      return view('pertanyaan.show', compact('question'));
    }

    public function edit($pertanyaan_id)
    {
      $question = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
      return view('pertanyaan.edit', compact('question'));
    }

    public function update($pertanyaan_id, Request $request)
    {
      $request->validate([
        'judul' => 'required|unique:pertanyaan',
        'isi' => 'required'
      ]);

      $query = DB::table('pertanyaan')
                  ->where('id', $pertanyaan_id)
                  ->update([
                    'judul' => $request['judul'],
                    'isi' => $request['isi']
                  ]);

      return redirect('/pertanyaan')->with('success', 'Berhasil update pertanyaan!');
    }

    public function destroy($pertanyaan_id)
    {
      $query = DB::table('pertanyaan')->where('id', $pertanyaan_id)->delete();
      return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil dihapus!');
    }
}
